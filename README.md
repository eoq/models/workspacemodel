# EOQ Workspace MDB Meta-model

This is the meta-model of the Workspace MDB of EOQ. It is defined in the ECORE format and can be edited with [Essential Object Query (EOQ)](https://gitlab.com/eoq/essentialobjectquery)  or the [Eclipse Modelling Framework (EMF)](). 

The Workspace MDB makes access to an arbitrary number of user-models with the same or different meta-models. It organizes access to models like a file system hierarchically, i.e. models are members of directories. Directories can contain other directories.

# EOQ

[Essential Object Query (EOQ)](https://gitlab.com/eoq/essentialobjectquery) is a language to interact remotely and efficiently with object-oriented models, i.e. domain-specific models. It explicitly supports the search for patterns, as used in model transformation languages. Its motivation is an easy to parse and deterministically behaving query structure, but as high as possible efficiency and flexibility. EOQ’s capabilities and semantics are similar to the Object-Constraint-Language (OCL), but it supports in addition transactional model modification, change events, and error handling.  

Main Repository: https://gitlab.com/eoq/essentialobjectquery

EOQ user manual: https://gitlab.com/eoq/doc 

